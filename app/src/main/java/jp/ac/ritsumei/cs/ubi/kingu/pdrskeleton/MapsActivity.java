/*
 * Copyright (C) 2008-2015 Ritsumeikan University Nishio Laboratory All Rights Reserved.
 */

package jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.sensor.SensorHardware;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, PDRListener {

    private GoogleMap mMap;
    private PDRCalculator pdrCalculator;
    private SensorHardware sensorHardware;

    private double previousLatitude = 0.0;
    private double previousLongitude = 0.0;
    private Marker nowPositionMarker = null;
    private ArrayList<Polyline> trajectoryPolyLines = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        sensorHardware = new SensorHardware(this);

        pdrCalculator = new PDRCalculator(sensorHardware);
        pdrCalculator.addListener(this);
        startPDR(pdrCalculator);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setCameraPosition(Const.RITS_CC_LATITUDE, Const.RITS_CC_LONGITUDE, Const.DEFAULT_CAMERA_ZOOM);
    }

    public void setCameraPosition(double latitude, double longitude, int zoom) {
        CameraPosition nowCamera = mMap.getCameraPosition();
        CameraPosition camera = new CameraPosition(
                new LatLng(latitude, longitude),
                zoom,
                nowCamera.tilt,
                nowCamera.bearing
        );
        setCameraPosition(camera);
    }

    public void setCameraPosition(CameraPosition cameraPosition) {
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void startPDR(PDRCalculator pdrCalculator) {
        previousLatitude = Const.RITS_CC_LATITUDE;
        previousLongitude = Const.RITS_CC_LONGITUDE;
        pdrCalculator.startPDR(
                Const.RITS_CC_LATITUDE, Const.RITS_CC_LONGITUDE,
                Const.OSAKA_STATION_LATITUDE, Const.OSAKA_STATION_LONGITUDE,
                Const.DEFAULT_STEP_LENGTH
        );
    }

    @Override
    public void onPDRChanged(double latitude, double longitude, double directionDegree) {
        setNowPosition(latitude, longitude);
        drawTrajectory(latitude, longitude);

        previousLatitude = latitude;
        previousLongitude = longitude;

        Log.d("onPDRChanged", "latitude: " + latitude + " longitude: " + longitude + " directionDegree: " + directionDegree);
    }

    private void setNowPosition(double latitude, double longitude) {
        if (nowPositionMarker != null) {
            nowPositionMarker.remove();
        }

        nowPositionMarker = mMap.addMarker(
                new MarkerOptions().position(new LatLng(latitude, longitude))
        );
    }

    private void drawTrajectory(double latitude, double longitude) {
        Polyline polyline = mMap.addPolyline(new PolylineOptions()
                .add(new LatLng(previousLatitude, previousLongitude))
                .add(new LatLng(latitude, longitude))
                .color(Color.BLACK)
                .width(3.0f));

        trajectoryPolyLines.add(polyline);
    }

}
