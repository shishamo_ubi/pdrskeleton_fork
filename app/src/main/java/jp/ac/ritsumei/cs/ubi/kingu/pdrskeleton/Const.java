/*
 * Copyright (C) 2008-2015 Ritsumeikan University Nishio Laboratory All Rights Reserved.
 */

package jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton;

public class Const {
    public final static double RITS_CC_LATITUDE = 34.97977501;
    public final static double RITS_CC_LONGITUDE = 135.96373915;
    public final static double OSAKA_STATION_LATITUDE = 34.70046472;
    public final static double OSAKA_STATION_LONGITUDE = 135.49728256;
    public final static double DEFAULT_STEP_LENGTH = 75.0;
    public final static int DEFAULT_CAMERA_ZOOM = 19;
    private Const() {
    }
}
