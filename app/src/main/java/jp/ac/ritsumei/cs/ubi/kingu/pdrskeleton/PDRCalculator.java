/*
 * Copyright (C) 2008-2015 Ritsumeikan University Nishio Laboratory All Rights Reserved.
 */

package jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton;

import java.util.ArrayList;

import jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.pdr.DirectionCalculator;
import jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.pdr.PeakStepDetector;
import jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.pdr.StepListener;
import jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.sensor.SensorInterface;
import jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.sensor.SensorLayer;
import jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.sensor.object.Accelerometer;
import jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.sensor.object.Gyro;

public class PDRCalculator implements StepListener, SensorInterface {

    // Conversion of coordinate(circumference of the Earth)
    private final double RX = 40076500;
    private final double RY = 40008600;

    private PeakStepDetector peakStepDetector;
    private DirectionCalculator directionCalculator;

    private ArrayList<PDRListener> pdrListeners = new ArrayList<>();

    private double latitude;
    private double longitude;
    private double stepLength;

    private SensorLayer sensorLayer;

    public PDRCalculator(SensorLayer sensorLayer) {
        this.sensorLayer = sensorLayer;
        sensorLayer.addListener(this);
    }

    public void addListener(PDRListener listener) {
        this.pdrListeners.add(listener);
    }

    /**
     * Start PDR
     * Setting initial point and the next point for indicating direction, this method starts PDR from the point and to the direction.
     *
     * @param startLatitude      Initial latitude
     * @param startLongitude     Initial longitude
     * @param directionLatitude  Second initial latitude
     * @param directionLongitude Second initial longitude
     * @param stepLength         Step length
     */
    public void startPDR(double startLatitude, double startLongitude, double directionLatitude, double directionLongitude, double stepLength) {
        this.latitude = startLatitude;
        this.longitude = startLongitude;
        this.stepLength = stepLength;

        this.peakStepDetector = new PeakStepDetector(
                1.0f,
                0.4f
        );
        this.peakStepDetector.addListener(this);

        double[] gyroOffsets = {0.0, 0.0, 0.0};
        this.directionCalculator = new DirectionCalculator(gyroOffsets);
        this.directionCalculator.setDegreesDirection(
                calculateStartDirectionDegree(
                        startLatitude,
                        startLongitude,
                        directionLatitude,
                        directionLongitude
                )
        );

        sensorLayer.startSensor();
    }

    /**
     * Stop PDR
     */
    public void stopPDR() {
        sensorLayer.stopSensor();
    }

    /**
     * This is Callback called when PeakStepDetector, library we made, detects a step.
     *
     * @param time Time (nanosecond)
     */
    @Override
    public void onStep(long time) {
        double directionRadian = this.directionCalculator.getRadiansDirection();
        calculatePosition(directionRadian, this.stepLength);

        for (PDRListener pdrListener : this.pdrListeners) {
            pdrListener.onPDRChanged(this.latitude, this.longitude, this.directionCalculator.getDegreesDirection());
        }
    }

    /**
     * This method calculates next point from the given direction and step length.
     *
     * @param direction  Direction[rad]
     * @param stepLength Step length
     */
    private void calculatePosition(double direction, double stepLength) {
        this.latitude += (stepLength * Math.sin(direction) / 100 / (RX / 360));
        this.longitude += (stepLength * Math.cos(direction) / 100 / (RY * Math.cos(Math.toRadians(latitude)) / 360));
    }

    /**
     * Method that calculates direction from current point and the next point.
     *
     * @param startLatitude      Current latitude
     * @param startLongitude     Current longitude
     * @param directionLatitude  Latitude at next point
     * @param directionLongitude Longitude at next point
     * @return 方向/Direction
     */
    private float calculateStartDirectionDegree(double startLatitude, double startLongitude, double directionLatitude, double directionLongitude) {
        double y = Math.cos(directionLongitude * Math.PI / 180.0) * Math.sin(directionLatitude * Math.PI / 180.0 - startLatitude * Math.PI / 180.0);
        double x = Math.cos(startLongitude * Math.PI / 180.0) * Math.sin(directionLongitude * Math.PI / 180.0) - Math.sin(directionLongitude * Math.PI / 180.0) * Math.cos(directionLongitude * Math.PI / 180.0) * Math.cos(directionLatitude * Math.PI / 180.0 - startLatitude * Math.PI / 180.0);

        double direction = 180.0 * Math.atan2(y, x);
        if (direction < 0.0) {
            direction += 360.0;
        }
        return (float) direction;
    }

    @Override
    public void onAccelerometerChanged(Accelerometer accelerometer) {
        float values[] = new float[]{accelerometer.getX(), accelerometer.getY(), accelerometer.getZ()};
        this.peakStepDetector.detectStepAndNotify(values, accelerometer.getTime());
        this.directionCalculator.calculateLean(values);
    }

    @Override
    public void onGyroChanged(Gyro gyro) {
        float values[] = new float[]{gyro.getX(), gyro.getY(), gyro.getZ()};
        this.directionCalculator.calculateDirection(values, gyro.getTime());
    }

}

