/*
 * Copyright (C) 2008-2015 Ritsumeikan University Nishio Laboratory All Rights Reserved.
 */
package jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.pdr;

/**
 * Used for receiving notifications from the LeaveDetector when step is detected.
 *
 * @author sacchin
 */
public interface StepListener {

    /**
     * Called when the step is detected.
     */
    void onStep(long time);
}