/*
 * Copyright (C) 2008-2015 Ritsumeikan University Nishio Laboratory All Rights Reserved.
 */

package jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.sensor;

import jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.sensor.object.Accelerometer;
import jp.ac.ritsumei.cs.ubi.kingu.pdrskeleton.sensor.object.Gyro;

public interface SensorInterface {
    void onAccelerometerChanged(Accelerometer accelerometer);

    void onGyroChanged(Gyro gyro);
}
