# Add your API key
Replace "your_api_key" with an API key generated using [these instructions](https://developers.google.com/maps/documentation/android-api/start#step_4_get_a_google_maps_api_key2)

# Copyright
Copyright (C) 2008-2015 Ritsumeikan University Nishio Laboratory All Rights Reserved.
